import os
from setuptools import find_packages, setup
with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()
# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))
setup(
    name='markbox-plugin-peer-marked',
    version='0.0.1',
    packages=find_packages(),
    install_requires=[],
    include_package_data=True,
    license='None',
    description='An optional markbox type allowing students to mark one-anothers\' work',
    long_description=README,
    url='http://example.com',
    author='James Lee',
    author_email='j584lee@uwaterloo.ca',
    classifiers=[
        'Intended Audience :: Developers',
        'License :: Unlicense',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.7',
    ],
)
