import math

def make_config(data):
    page_size = 'letter'
    num_questions = 180 if page_size == 'letter' else 264
    scan_bar_location = 742 if page_size == 'letter' else 962
    
    config = {
        'markers': [
            (46, 32, 25),
            (544, 32, 25),
            (40, scan_bar_location, 25),
            (550, scan_bar_location, 25),
        ],
        'page_size': page_size,
        "components": {
            "test_taker": {
                "type": "HTMLBlock",
                "x": 40,
                "y": 52,
                "config": {
                    "capture": True,
                    "content": "",
                    "width": 120,
                    "height": 69,
                    "style": {}
                }
            },
            "marker": {
                "type": "HTMLBlock",
                "x": 452,
                "y": 52,
                "config": {
                    "capture": True,
                    "content": "",
                    "width": 120,
                    "height": 69,
                    "style": {}
                }
            },
            "test_taker_name": {
                "type": "HTMLBlock",
                "x": 40,
                "y": 52,
                "config": {
                    "content": "Test Taker Name:",
                    "width": 100,
                    "height": 10,
                    "style": {}
                }
            },
            "marker_name": {
                "type": "HTMLBlock",
                "x": 452,
                "y": 52,
                "config": {
                    "content": "Marker Name:",
                    "width": 100,
                    "height": 10,
                    "style": {}
                }
            },
            "test_taker_name_block": {
                "type": "HTMLBlock",
                "x": 40,
                "y": 65,
                "config": {
                    "capture": True,
                    "content": "",
                    "width": 120,
                    "height": 20,
                    "style": {
                        "border": "0.6pt solid black"
                    }
                }
            },
            "marker_name_block": {
                "type": "HTMLBlock",
                "x": 452,
                "y": 65,
                "config": {
                    "capture": True,
                    "content": "",
                    "width": 120,
                    "height": 20,
                    "style": {
                        "border": "0.6pt solid black"
                    }
                }
            },
            "test_taker_student_id_label": {
                "type": "HTMLBlock",
                "x": 40,
                "y": 88,
                "config": {
                    "content": "Student Code",
                    "width": 100,
                    "height": 10,
                    "style": {}
                }
            },
            "marker_student_id_label": {
                "type": "HTMLBlock",
                "x": 452,
                "y": 88,
                "config": {
                    "content": "Student Code",
                    "width": 100,
                    "height": 10,
                    "style": {}
                }
            },
            "test_taker_student_number": {
                "type": "NumberInput",
                "x": 40,
                "y": 101,
                "config": {
                    "capture": True,
                    "box_height": 20,
                    "box_width": 15,
                    "y_margin": 2,
                    "bubble_config": {
                    "width": 8,
                    "height": 11
                    }
                }
            },
            "marker_student_number": {
                "type": "NumberInput",
                "x": 452,
                "y": 101,
                "config": {
                    "capture": True,
                    "box_height": 20,
                    "box_width": 15,
                    "y_margin": 2,
                    "bubble_config": {
                    "width": 8,
                    "height": 11
                    }
                }
            },
            "_code": {
                "type": "HTMLBlock",
                "x": 155,
                "y": 40,
                "config": {
                    "capture": True,
                    "content": "\n            <img style=\"width: 80pt; height: auto;\" src=\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20width%3D%2729%27%20height%3D%2729%27%20class%3D%27segno%27%3E%3Cpath%20stroke%3D%27%23000%27%20class%3D%27qrline%27%20d%3D%27M4%204.5h7m4%200h1m2%200h7m-21%201h1m5%200h1m1%200h4m2%200h1m5%200h1m-21%201h1m1%200h3m1%200h1m1%200h1m1%200h3m1%200h1m1%200h3m1%200h1m-21%201h1m1%200h3m1%200h1m1%200h4m2%200h1m1%200h3m1%200h1m-21%201h1m1%200h3m1%200h1m1%200h1m1%200h1m1%200h1m1%200h1m1%200h3m1%200h1m-21%201h1m5%200h1m1%200h2m1%200h2m1%200h1m5%200h1m-21%201h7m1%200h1m1%200h1m1%200h1m1%200h7m-12%201h2m1%200h1m-11%201h1m2%200h6m2%200h1m1%200h5m-18%201h1m1%200h1m2%200h2m1%200h5m3%200h1m-17%201h2m1%200h2m3%200h3m5%200h2m-19%201h1m1%200h1m3%200h2m1%200h1m2%200h2m1%200h1m1%200h1m1%200h1m-21%201h2m1%200h4m2%200h1m1%200h1m1%200h1m3%200h1m-10%201h5m4%200h1m1%200h2m-21%201h7m1%200h3m2%200h5m1%200h1m-20%201h1m5%200h1m1%200h3m1%200h3m3%200h3m-21%201h1m1%200h3m1%200h1m2%200h1m3%200h1m1%200h6m-21%201h1m1%200h3m1%200h1m2%200h2m2%200h1m-14%201h1m1%200h3m1%200h1m1%200h1m1%200h2m1%200h1m1%200h2m1%200h3m-21%201h1m5%200h1m2%200h1m2%200h1m2%200h1m1%200h2m1%200h1m-21%201h7m2%200h2m1%200h1m1%200h4m1%200h2%27%2F%3E%3C%2Fsvg%3E\">\n        ",
                    "width": 100,
                    "height": 100,
                    "style": {
                    "text-align": "center"
                    }
                }
            },
            "instructions": {
                "type": "HTMLBlock",
                "x": 240,
                "y": 49,
                "config": {
                    "content": "\n                    <ul style=\"margin: 0 0 0 -18pt\">\n                    <li style=\"margin-bottom: 5pt;\">Neatly enter your name and student number in the space provided.</li>\n                        <li style=\"margin-bottom: 5pt;\">Completely fill in bubbles. Partially filled bubbles may not be marked.</li>\n                    </ul>\n                    ",
                    "width": 192,
                    "height": 100,
                    "style": {}
                }
            },
            "divider": {
                "type": "HTMLBlock",
                "x": 40,
                "y": 260,
                "config": {
                    "content": "",
                    "width": 532,
                    "height": 476,
                    "style": {
                    "border": "1pt solid black"
                    }
                }
            },
            "question_container": {
                "type": "HTMLBlock",
                "x": 170,
                "y": 113,
                "config": {
                    "capture": True,
                    "content": "Scores",
                    "width": 272,
                    "height": 141,
                    "style": {
                        "line-height": '18pt',
                        "font-size": '12pt',
                        "text-align": "center",
                        "border": "1pt solid black"
                    }
                }
            }
        }
    }

    max_num_answer = 0
    for question in data['questions']:
        if question['num_answers'] > max_num_answer:
            max_num_answer = question['num_answers']
    
    if len(data['questions']) <= 10:
        questions = {
            'Q{}'.format(i): {
                'x': 190, 'y': 117+i*12,
                'type': 'MCQuestion',
                'config': {
                    'label': '{:03d}'.format(i),
                    'option_list': list(range(0, q['num_answers']+1)),
                    'label_width': 25,
                    'bubble_config': {
                        'width': 10,
                        'height': 10
                    }
                }
            }
            for i, q in enumerate(data['questions'], 1)
        }

    else:
        questions = {
            'Q{}'.format(i): {
                'x': 190+math.floor((i-1)/10)*136, 'y': 117+(i-math.floor((i-1)/10)*10)*12,
                'type': 'MCQuestion',
                'config': {
                    'label': '{:03d}'.format(i),
                    'option_list': list(range(0, q['num_answers']+1)),
                    'label_width': 25,
                    'bubble_config': {
                        'width': 10,
                        'height': 10
                    }
                }
            }
            for i, q in enumerate(data['questions'], 1)
        }

    config['components'].update(questions)
    print(config)

    return config
