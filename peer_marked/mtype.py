from markbox.markset_types.hand_marked import HandMarked
from PIL import Image, ImageDraw
from .config import make_config
from collections import OrderedDict
import segno
import csv
from django.http import HttpResponse


class PeerMarked(HandMarked):
    name = "Peer-marked Cover Sheet"
    short = "Peer-marked"
    config_link_text = 'Enter Test Versions'
    default_printing_options = OrderedDict([
    ])
    description_template = 'markset_types/peer_marked/description.html'
    setup_help_template = 'markset_types/peer_marked/setup_help.html'
    extracts = ['test_taker', 'marker']
    student_number_key = 'test_taker_student_number'

    default_config_data = dict(
        page_size='letter',
        option=0,
        questions=[],
    )
    config_settings = dict(
        page_sizes=['letter'],
        question_options=[
            {
                'title': 'One Column',
                'configs': {
                    'letter': {
                        'max_questions': 10,
                        'max_value': 15,
                        'cols': 1,
                    },
                }
            },

            {
                'title': 'Two Columns',
                'configs': {
                    'letter': {
                        'max_questions': 20,
                        'max_value': 5,
                        'cols': 2,
                    },
                }
            },
        ]
    )
    extra_correlations = [
        {
           'label': 'Marker',
           'helptext': 'example (optional) helptext',
           # Where the data will be looked for and stored in mark_data
           'key': 'marker_id',
           # A list of dicts with 'display' and 'value' keys
           'pool': lambda m: [{'display': f'{student.student_number} - {student.last_name}, {student.first_name}', 'value': student.student_number}for student in m.student_set.all()],
        }
    ]

    def process_scanbot_config(self, markset):
        # Ensure we've got the right page size
        # Get the first config
        config = make_config(markset.config_set.first().data)
        config['components']['_code']['config']['content'] = '''
            <img style="width: 80pt; height: auto;" src="{}">
        '''.format(
            segno.make('MARKBOX', micro=False).svg_data_uri(),
        )
        return config

    def correlate_config(self, markset, barcodes, mark_data):
        marker_number = mark_data.get('marker_student_number', {}).get('number')
        if markset.student_set.all().filter(student_number=marker_number).exists():
            mark_data.update(marker_id=marker_number)
        if markset.config_set.count() == 1:
            return markset.config_set.first()

    def extra_correlation(self, markset, student, config, mark_data):
        '''Modify our mark data to have stringed selections. Numbers
        break the marking page'''
        for key, val in mark_data.items():
            if key.startswith('Q'):
                mark_data[key]['selections'] = [str(x) for x in val['selections']]


    def get_gradesheet(self, request, markset):
        '''Return a fileresponse or http response of a gradesheet
        this defaults to generating a CSV of each student's score'''
        students = {
            s.student_number: s
            for s in markset.student_set.all()
        }
        tests = markset.good_tests
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] =\
            'attachment; filename="{}-gradesheet.csv"'\
            .format(markset.title)
        writer = csv.writer(response)
        writer.writerow(
            [
                'Test Taker Student ID',
                'Test Taker Email',
                'Test Taker First Name',
                'Test Taker Last Name',
                'Score',
                'Marker Student ID',
                'Marker Email',
                'Marker First Name',
                'Marker Last Name',
            ]
        )
        for test in tests:
            marker_number = test.data.get('marker_id')
            marker = students[marker_number]
            writer.writerow(
                [
                    test.student.student_number,
                    test.student.email_address,
                    test.student.first_name,
                    test.student.last_name,
                    int(test.score),
                    marker.student_number,
                    marker.email_address,
                    marker.first_name,
                    marker.last_name,
                ]
            )
        return response

    def annotate_bubble_sheet(self, page, config, aoi):
        im = super().annotate_bubble_sheet(page, config, aoi)
        marker_info_loc = (1796, 204)
        marker_info_size = (508, 825)
        dr = ImageDraw.Draw(im)
        dr.rectangle(
            [
                marker_info_loc, 
                (
                    marker_info_loc[0] + marker_info_size[0],
                    marker_info_loc[1] + marker_info_size[1],
                )
            ],
            fill="#ffffff"
        )
        #i = im.crop((marker_info_loc[0], marker_info_loc[1], marker_info_loc[0]+marker_info_size[0], marker_info_loc[1]+marker_info_size[1]))
        #i = i.resize([int(x/25) for x in marker_info_size], Image.BILINEAR)
        #i = i.resize(marker_info_size, Image.NEAREST)
        #im.paste(i, marker_info_loc)
        return im
